var localEvents = browser.storage.local.get();

localEvents.then(results => {
    console.log(results);
    var a = document.getElementById("download");
    var file = new Blob([JSON.stringify(results)], {type: 'text/plain'});
    a.href = URL.createObjectURL(file);
    a.download = 'graph.json';

    var btn = document.getElementById("delete");
    btn.addEventListener("click", (evt) => {
        browser.storage.local.set({vertices: {}, edges: {}});
    });
});


