function openMyPage() {
   browser.tabs.create({
     "url": "/index.html"
   });
}

browser.browserAction.onClicked.addListener(openMyPage);

// Global state about tabs (should use timeit information)
var tabUrls = {}  // tabId -> url
var previouslyActive = undefined;
var currentActive = undefined;
var pendingNavigations = {};  // tabId -> pendingNavigation
// Add current tab information
function loadCurrentTabs(tabs) {
    for (tab in tabs) {
        tabUrls[tab.id] = tab.url;
        if (tab.active) {
            previouslyACtive = tab.id;
        }
    }
}
// Initialize current tabs
browser.tabs.query({}).then(loadCurrentTabs);
// Get local storage
var localEvents = browser.storage.local.get();
// Listen for tabs
browser.tabs.onActivated.addListener(evt => {
    previouslyActive = evt.previousTabId;
    currentActive = evt.tabId;
});
// easy function to update the current url of a tab
function updateUrl(tab) {
    tabUrls[tab.id] = tab.url;
}
// Try and get the previous tab accessed
// TODO: use timeme info
function getPreviousTab(tabId) {
    if (typeof currentlyActive !== 'undefined') {
        if (currentlyActive !== tab.id) {
            return currentlyActive;
        }
    } else if (typeof previouslyActive !== 'undefined') {
        return previouslyActive;
    }
    return undefined;
}
// How we map tab information into our simplified scheme 
var mapping = {
    "typed": "address",
    "link": "link",
    "address": "address",
    "generated": "address"
};
localEvents.then(results => {
  // Initialize the saved stats if not yet initialized.
  if (!results.stats) {
    results = {
      vertices: {},
      edges: {}
    };
  }

  // Monitor when we start navigating to it
  browser.webNavigation.onCommitted.addListener((evt) => {
    if (evt.frameId !== 0) {
      return;
    }
    console.log("Navigation EVENT");
    console.log(evt);
    let newtab = !tabUrls.hasOwnProperty(evt.tabId);
    // We're going to hold off on updating tabUrl until later,
    // but we will add it to list of pending navigations
    if (mapping[evt.transitionType]) {
        pendingNavigations[evt.tabId] = {
            newtab: newtab,
            type: mapping[evt.transitionType],
            timeIds: {}
        };
    }
  });

  
  // Listen to whenever the URL is updated
  browser.tabs.onUpdated.addListener((tabId, changedInfo, tab) => {
      if (changedInfo.status !== "complete" || tab.url === "about:blank") {
          return;
      }
      console.log("Logging new url");
      console.log(tab.url);
      if (!results.vertices.hasOwnProperty(tab.url)) {
        results.vertices[tab.url] = {
            title: tab.title,
            lastAccessed: tab.lastAccessed,
            timeIds: {}
        };
      }
      // If we got here from a link or from an address.
      if (pendingNavigations.hasOwnProperty(tab.id)) {
          var prevUrl = undefined;
          let navigation = pendingNavigations[tab.id];
          // if got here using a new tab
          if (navigation.newtab) {
              // if we got here using a link, there should be an opener
              if (tab.openerTabId) {
                  prevUrl = tabUrls[tab.openerTabId];
              } else {
                  // otherwise we have to use the prevously active
                  if (getPreviousTab(tab.id)) {
                      prevUrl = tabUrls[getPreviousTab(tab.id)];
                  }
              }
          } else {
              prevUrl = tabUrls[tab.id];
          }
          if (typeof prevUrl !== 'undefined' && prevUrl !== tab.url) {
            edge = {
                source: prevUrl,
                target: tab.url,
                timestamp: tab.lastAccessed,
                type: navigation.type,
                newtab: navigation.newtab
            };
            if (!results.edges.hasOwnProperty(prevUrl)) {
                results.edges[prevUrl] = [];
            }
            results.edges[prevUrl].push(edge);
          }
          delete pendingNavigations[tab.id];
      }
      tabUrls[tab.id] = tab.url;
      // Persist the updated stats.
      browser.storage.local.set(results);
  }, {
      properties: ["status"]
  });
  // Listen for when the page actually sends us some statistics
  browser.runtime.onMessage.addListener(notify);
    
  function notify(message) {
      if (message.type === "initial") {
          if (!results.vertices.hasOwnProperty(message.url)) {
              results.vertices[message.url] = {
                  title: message.title,
                  lastAccessed: message.time,
                  timeIds: {}
              };
          }
          console.log("Initial: " + message.url + " | " + message.id);
          console.log(results.vertices[message.url]);
          results.vertices[message.url].timeIds[message.id] = {
              starts: [],
              ends: [],
              totalTime: 0
          };
      } else if (message.type === "start") {
          console.log("Start: " + message.url + " | " + message.id);
          results.vertices[message.url]
              .timeIds[message.id]
              .starts.push(message.time);
          results.vertices[message.url]
              .timeIds[message.id].totalTime = message.timeSpent;
          results.vertices[message.url].lastAccessed = message.time;
      } else if (message.type === "end") {
          console.log("End: " + message.url + " | " + message.id);
          results.vertices[message.url]
              .timeIds[message.id]
              .ends.push(message.time);
          results.vertices[message.url]
              .timeIds[message.id].totalTime = message.timeSpent;
          results.vertices[message.url].lastAccessed = message.time;
      }
      browser.storage.local.set(results);
      console.log("Finished " + message.type);
  }

  console.log("Initialized!");

});
