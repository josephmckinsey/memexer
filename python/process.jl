### A Pluto.jl notebook ###
# v0.11.4

using Markdown
using InteractiveUtils

# ╔═╡ 27a1b60e-e0cd-11ea-0631-299fc6753872


# ╔═╡ 46e984de-e0cc-11ea-2c8c-7b575fc73199
using LightGraphs

# ╔═╡ 41f2a766-e0cb-11ea-1195-a98ce7658f79
import JSON

# ╔═╡ 69f56a48-e0cb-11ea-01d5-81865f7ef029
begin
	graph_text = open("graph.json", "r") do f
		read(f, String)
	end
	graph_json = JSON.parse(graph_text)
end

# ╔═╡ 77ce1fa6-e0cc-11ea-1daa-793947473eb0
graph = SimpleDiGraph()

# ╔═╡ Cell order:
# ╠═41f2a766-e0cb-11ea-1195-a98ce7658f79
# ╠═69f56a48-e0cb-11ea-01d5-81865f7ef029
# ╠═46e984de-e0cc-11ea-2c8c-7b575fc73199
# ╠═77ce1fa6-e0cc-11ea-1daa-793947473eb0
# ╠═27a1b60e-e0cd-11ea-0631-299fc6753872
