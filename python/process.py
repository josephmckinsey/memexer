import json
import networkx as nx

with open('graph.json') as f:
    j = json.load(f)

g = nx.MultiDiGraph()

for url, attributes in j['vertices'].items():
    if url.startswith('about:blank'):
        continue
    g.add_node(url, url=url, **attributes)

for v in j['edges'].values():
    for edge in v:
        if edge['source'].startswith('about:blank'):
            continue
        if edge['target'].startswith('about:blank'):
            continue
        g.add_edge(edge['source'], edge['target'],
                timestamp=edge['timestamp'],
                type=edge['type'],
                newtab=edge['newtab'])
## Do some REAL plotting

import holoviews as hv
from holoviews import opts
hv.extension('bokeh')
from bokeh.plotting import show

defaults = dict(width=1700, height=1000)
hv.opts.defaults(
    opts.EdgePaths(**defaults), opts.Graph(**defaults), opts.Nodes(**defaults))

data = []

for n, v in g.nodes.items():
    data.append((v['lastAccessed'], n))

data.sort()
for i in range(len(data)):
    data[i] = (i, data[i][1])

import math

positions = {j: (i*math.cos(i), i*math.sin(i)) for i, j in data}

hgraph = hv.Graph.from_networkx(g, 
        lambda g: nx.drawing.spring_layout(g, iterations=20, k=5, pos=positions)).opts(
        tools=['hover'], directed=True, arrowhead_length=0.008
        )
what = hv.render(hgraph)
show(what) # is going on!

#from bokeh.io import output_file, show
#from bokeh.models import (BoxZoomTool, Circle, HoverTool,
                          #MultiLine, Plot, Range1d, ResetTool,)
#from bokeh.palettes import Spectral4
#from bokeh.plotting import from_networkx

# Show with Bokeh
#plot = Plot(plot_width=1500, plot_height=800,
            #x_range=Range1d(-1.1, 1.1), y_range=Range1d(-1.1, 1.1))
#plot.title.text = "User History"

#node_hover_tool = HoverTool(tooltips=[("url", "@url"), ("title", "@title")])
#plot.add_tools(node_hover_tool, BoxZoomTool(), ResetTool())

#graph_renderer = from_networkx(g, nx.spring_layout, scale=1, center=(0, 0))

#graph_renderer.node_renderer.glyph = Circle(size=15, fill_color=Spectral4[0])
#graph_renderer.edge_renderer.glyph = MultiLine(line_color="black", line_alpha=0.8, line_width=1)
#plot.renderers.append(graph_renderer)

#output_file("graph.html")
#show(plot)
