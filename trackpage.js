console.log(document.referrer);

//window.addEventListener("click", notifyExtension);

//function notifyExtension(e) {
//  if (e.target.tagName != "A") {
// return;
// }
// browser.runtime.sendMessage({"url": e.target.href});
// }
try {
    TimeMe.initialize({
        currentPageName: document.title, // current page
        idleTimeoutInSeconds: 10 // seconds
    });
    console.log("Successfully Initialized Tracking");
} catch (e) {
    console.log("Failed to initialize tracking :(");
    console.log(e);
}

var id = Math.floor(Math.random() * 1000000);

function sendInitial() {
    console.log("Sending Initial Data");
    browser.runtime.sendMessage({
        type: "initial",
        url: window.location.href,
        time: Date.now(),
        prevUrl: document.referrer,
        title: document.title,
        id: id
    });
        
}

function sendStart() {
    console.log("Start Event. Duration:");
    var currentTime = TimeMe.getTimeOnCurrentPageInSeconds();
    console.log(currentTime);
    browser.runtime.sendMessage({
        type: "start",
        url: window.location.href,
        timeSpent: currentTime,
        time: Date.now(),
        id: id
    });
}

function sendEnd() {
    console.log("End Event. Duration:");
    var currentTime = TimeMe.getTimeOnCurrentPageInSeconds();
    console.log(currentTime);
    browser.runtime.sendMessage({
        type: "end",
        url: window.location.href,
        timeSpent: currentTime,
        time: Date.now(),
        id: id
    });
}

sendInitial();
console.log("First Start!")
sendStart();

TimeMe.callWhenUserReturns(sendStart);
TimeMe.callWhenUserLeaves(sendEnd);

//window.addEventListener('unload', sendEnd);
